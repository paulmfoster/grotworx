#!/usr/bin/env php
<?php

/**
 * Upgrade an installation which includes Grotworx.
 *
 * This program must be run from the grotworx home directory.
 */

if (count($argv) != 2) {
    // usage
    die("Usage: upgrade destination-directory\n");
}

$destdir = $argv[1];
if (!file_exists($destdir)) {
    die("Destination directory doesn't exist. You must install Grotworx at destination first.\n");
}
$files = file('MANIFEST', FILE_IGNORE_NEW_LINES);

foreach ($files as $file) {
    $destfile = $destdir . DIRECTORY_SEPARATOR . $file;
    $ddir = dirname($destfile);
    if (!file_exists($ddir)) {
        mkdir($ddir, 0755, true);
    }
    $result = @copy($file, $destdir . DIRECTORY_SEPARATOR . $file);
    $rstring = $result ? 'SUCCEEDED' : 'FAILED';
    echo 'Copying ' . $file . ' to ' . $destfile . '... ' . $rstring . "\n";
}

