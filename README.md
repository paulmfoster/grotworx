# Grotworx

This is a lightweight OOP framework for building a website or web
application. The framework itself is driven by less than five files. The
rest of the files included are utility functions and classes to make the
job of the programmer easier.

## Directory Structure

The following is the directory structure for Grotworx:

```
app/
    config/
    controllers/
    data/
    images/
    models/
    printq/
    views/
doc/
system/
    includes/
    libraries/
index.php
```

## The app/ Directory

This directory has placeholder directories for user files. They're names in
obvious ways. A given user may not need all these, but they don't take up
much space by default. They could mostly be deleted or renamed if the user
prefers. However, there are two exceptions. The `controllers` directory
contains the file `welcome.php`, which is the default controller. Since
this is the default controller, it should not be removed. However, the user
may modify it as desired. Likewise, there is an `index()` method within
that controller which should not be removed. But again, it can be modified.

## The system/ Directory

This contains a variety of class files and files which contain useful
utility functions. The following are critical to the operation of the
framework, and should not be removed:

```
system/libraries/controller.php
system/libraries/router.lib.php
```

Other files may be of use to the programmer, but may be removed or edited.
However, note that upgrades to Grotworx will overwrite any changes to these
files and replace any missing files.

### system/libraries/controller.php

This is the parent controller from which all user controllers inherit. It
contains a `model()` method which allows the user to easily instantiate
whatever model they choose (in the `models` directory). If needed, it does
allow parameters to be passed into the model, such as a database handle. In
addition, this class contains a `view()` method which can be called with or
without parameters. This includes the view file (in the `views` directory)
specified.

### system/libraries/router.lib.php

This is a router class used by the `index.php` file to parse and route the
system to the proper controller. See below for a discussion of URLs.

## The doc/ Directory

The `doc/` directory contains PHPDoc generated documentation of the
individual files and classes comprising Grotworx. These files aren't needed
in a standard installation. They are for the benefit of programmers who
want to use the included classes. To see this documentation, surf to

```
http://example.com/doc/index.html
```

If you don't need the files in this directory, or you've finished with
them, feel free to delete the directory in any live installation.

## The index.php File

This is the first page any visits to your site will hit. It sets various
constants, pulls in error functions, message functions (to inform users
about events, checks for and includes the bootstap file, if any (see
later), and then instantiates the router class. The router class parses the
URL and directs execution to the proper place.

## The app/bootstrap.php File

The `index.php` file, before it launches the controller, checks for the
existence of an `app/bootstrap.php` file. If found, it will `include()` it.
The user can put whatever they like in that file. You can define constants,
include files, instantiate classes, whatever. After this file is included,
the router class is instantiated.

## URLs

The basic form for URLs is:

```
index.php?url=controller/method
```

Typical URLs will look like this: `index.php?url=dogs/poodle`. The server
hits the `index.php` file, and the router class parses the remainder of the
URL. In the above case, the server would expect to see a file called
`app/controllers/dogs.php`, which would contain a controller class. The
class would extend the parent controller class (see the "welcome"
controller for an example). In that class, it would expect to see a method
called `poodle()`, which would call in the model and view.

A URL which looks like this: `index.php?url=dogs` will route the user to
the same controller. Since the URL doesn't specify anything after "dogs",
the router expects a default method called `index()` to exist in the
controller.

The URL `index.php` gets the user to the default "welcome" controller and
the default "index" method.

## Installation

Just copy or `git clone` Grotworx to wherever your site will live. Then
populate its `app/` directory with your files. The "welcome" controller and
its associated view (`app/views/index.view.php`) are examples of how to
structure a Grotworx site. Incidentally, when upgrading, the welcome
controller and its view are not overwritten. This allows the user to
customize these without interference. However, be aware that any edits to
the `index.php` file the user makes will be lost on upgrades, as this file
will be replaced.

## Licensing, Credits, etc.

I wrote this to satisfy my own needs. You're free to use it as you like. If
you need a license for it, use the GPL version 2. I'm not responsible for
your use of this package or any consequences from its use. If you modify
it, I'd like to know about it. I might want to fold in your modifications
(and give you credit). If you find bugs, let me know via Gitlab.

Paul M. Foster <paulf@quillandmouse.com>
https://gitlab.com/paulmfoster

